﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_API_REST.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static FicProyecto_API_REST.Models.FicCarrera;

namespace FicProyecto_API_REST.Controllers
{
    
    public class FicEspecialidadesController : ControllerBase
    {
        private readonly FicDBContext FicLoDBContext;

        public FicEspecialidadesController(FicDBContext FicPaDBContext)
        {

            FicLoDBContext = FicPaDBContext;

        }
        [HttpGet]
        [Route("api/especialidades/getespecialidades/")]
        public async Task<ActionResult> GetEspecialidades()
        {
            try
            {
                var response = FicLoDBContext.eva_cat_especialidades.ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }
        [HttpPost]
        [Route("api/especialidades/addespecialidad")]
        public ContentResult addespecialidad([FromBody] eva_cat_especialidades value)
        {
            if (value != null)
            {
                try
                {
                    try
                    {
                        var responsemax = FicLoDBContext.eva_cat_especialidades.Max(x => x.IdEspecialidad);
                        if (responsemax != 0)
                        {
                            value.IdEspecialidad = (Int16)(responsemax + 1);
                        }
                        else
                        {
                            value.IdEspecialidad = 1;
                        }
                    }
                    catch (Exception e)
                    {
                        value.IdEspecialidad = 1;
                    }

                    value.UsuarioReg = "FicIbarra";
                    value.UsuarioMod = "FicIbarra";
                    FicLoDBContext.eva_cat_especialidades.Add(value);
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpGet("{id}")]
        [Route("api/especialidades/especialidadesdelete/")]
        public async Task<ActionResult> especialidadesdelete(Int16 id)
        {
            try
            {
                eva_cat_especialidades a = new eva_cat_especialidades();
                a.IdEspecialidad = id;
                FicLoDBContext.Remove(a);
                var response = FicLoDBContext.SaveChanges();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }

        [HttpGet("{id}")]
        [Route("api/especialidades/especialidadesid/")]
        public async Task<ActionResult> especialidadesid(Int16 id)
        {
            try
            {
                var response = FicLoDBContext.eva_cat_especialidades.Where(x => x.IdEspecialidad == id).ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }
        [HttpPost]
        [Route("api/especialidades/especialidadesidupdate")]
        public ContentResult especialidadesidupdate([FromBody] eva_cat_especialidades value)
        {
            if (value != null)
            {
                try
                {
                    FicLoDBContext.eva_cat_especialidades.Update(value).Property(x => x.FechaReg).IsModified = false;
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpGet]
        [Route("api/especialidades/getespecarreras/")]
        public async Task<ActionResult> getespecarreras()
        {
            try
            {
                var response = from FicCED in FicLoDBContext.eva_carreras_especialidades
                                select new eva_carreras_especialidades
                                {
                                    IdCarrera = FicCED.IdCarrera,
                                    IdCarreraEspecilidad = FicCED.IdCarreraEspecilidad,
                                    IdEspecialidad = FicCED.IdEspecialidad,
                                    Activo = FicCED.Activo,
                                    Borrado = FicCED.Borrado,
                                    FechaFin = FicCED.FechaFin,
                                    FechaIni = FicCED.FechaIni,
                                    FechaReg = FicCED.FechaReg,
                                    FechaUltMod = FicCED.FechaUltMod,
                                    UsuarioMod = FicCED.UsuarioMod,
                                    UsuarioReg = FicCED.UsuarioReg,
                                    eva_cat_carreras = (
                                    from FG in FicLoDBContext.eva_cat_carreras
                                    where FG.IdCarrera == FicCED.IdCarrera
                                    select new eva_cat_carreras
                                    {
                                        DesCarrera = FG.DesCarrera,
                                        ClaveCarrera = FG.ClaveCarrera,
                                    }).First(),
                                    eva_cat_especialidades = (
                                    from FG in FicLoDBContext.eva_cat_especialidades
                                    where FG.IdEspecialidad == FicCED.IdEspecialidad
                                    select new eva_cat_especialidades
                                    {
                                        DesEspecialidad = FG.DesEspecialidad,
                                    }).First(),
                                };

                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }

        [HttpPost]
        [Route("api/especialidades/addespecialidadcarrera")]
        public ContentResult addespecialidadcarrera([FromBody] eva_carreras_especialidades value)
        {
            if (value != null)
            {
                try
                {
                    try
                    {
                        var responsemax = FicLoDBContext.eva_carreras_especialidades.Max(x => x.IdCarreraEspecilidad);
                        if (responsemax != 0)
                        {
                            value.IdCarreraEspecilidad = (Int16)(responsemax + 1);
                        }
                        else
                        {
                            value.IdCarreraEspecilidad = 1;
                        }
                    }
                    catch (Exception e)
                    {
                        value.IdCarreraEspecilidad = 1;
                    }
                    value.UsuarioReg = "FicIbarra";
                    value.UsuarioMod = "FicIbarra";
                    FicLoDBContext.eva_carreras_especialidades.Add(value);
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpGet("{id}")]
        [Route("api/especialidades/getespecarrerasid/")]
        public async Task<ActionResult> getespecarrerasid(Int16 id)
        {
            try
            {
                var response = (from FicCED in FicLoDBContext.eva_carreras_especialidades where FicCED.IdCarreraEspecilidad==id
                               select new eva_carreras_especialidades
                               {
                                   IdCarrera = FicCED.IdCarrera,
                                   IdCarreraEspecilidad = FicCED.IdCarreraEspecilidad,
                                   IdEspecialidad = FicCED.IdEspecialidad,
                                   Activo = FicCED.Activo,
                                   Borrado = FicCED.Borrado,
                                   FechaFin = FicCED.FechaFin,
                                   FechaIni = FicCED.FechaIni,
                                   FechaReg = FicCED.FechaReg,
                                   FechaUltMod = FicCED.FechaUltMod,
                                   UsuarioMod = FicCED.UsuarioMod,
                                   UsuarioReg = FicCED.UsuarioReg,
                                   eva_cat_carreras = (
                                   from FG in FicLoDBContext.eva_cat_carreras
                                   where FG.IdCarrera == FicCED.IdCarrera
                                   select new eva_cat_carreras
                                   {
                                       IdCarrera= FG.IdCarrera,
                                       DesCarrera = FG.DesCarrera,
                                       ClaveCarrera = FG.ClaveCarrera,
                                   }).First(),
                                   eva_cat_especialidades = (
                                   from FG in FicLoDBContext.eva_cat_especialidades
                                   where FG.IdEspecialidad == FicCED.IdEspecialidad
                                   select new eva_cat_especialidades
                                   {
                                       IdEspecialidad=FG.IdEspecialidad,
                                       DesEspecialidad = FG.DesEspecialidad,
                                   }).First(),
                               }).ToList();

                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("");
            }
        }
        [HttpGet("{id}")]
        [Route("api/especialidades/especialidadesecdelete/")]
        public async Task<ActionResult> especialidadesecdelete(Int16 id)
        {
            try
            {
                eva_carreras_especialidades a = new eva_carreras_especialidades();
                a.IdCarreraEspecilidad = id;
                FicLoDBContext.Remove(a);
                var response = FicLoDBContext.SaveChanges();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }


        [HttpPost]
        [Route("api/especialidades/especialidadesiECdupdate")]
        public ContentResult especialidadesiECdupdate([FromBody] eva_carreras_especialidades value)
        {
            if (value != null)
            {
                try
                {
                    FicLoDBContext.eva_carreras_especialidades.Update(value).Property(x => x.FechaReg).IsModified = false;
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }


        [HttpGet("{id}")]
        [Route("api/especialidades/GetEspecialidadesCarreraEC/")]
        public async Task<ActionResult> GetEspecialidadesCarreraEC(Int16 id)
        {
            try
            {
                var response = (from eva_cat_especialidades in FicLoDBContext.eva_cat_especialidades
                               from FicCDD in FicLoDBContext.eva_carreras_especialidades
                               where eva_cat_especialidades.IdEspecialidad == FicCDD.IdEspecialidad && FicCDD.IdCarrera == id
                               select new eva_cat_especialidades
                               {
                                   IdEspecialidad= eva_cat_especialidades.IdEspecialidad,
                                   DesEspecialidad = eva_cat_especialidades.DesEspecialidad,
                               });

                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }

        [HttpGet("{id}")]
        [Route("api/especialidades/GetEspecialidadesCarreraEC2/")]
        public async Task<ActionResult> GetEspecialidadesCarreraEC2(Int16 id)
        {
            try
            {
                var response = (from eva_cat_especialidades in FicLoDBContext.eva_cat_especialidades
                                from FicCDD in FicLoDBContext.eva_carreras_especialidades
                                where eva_cat_especialidades.IdEspecialidad == FicCDD.IdEspecialidad && FicCDD.IdCarrera == id
                                select new eva_carreras_especialidades
                                {
                                    IdCarreraEspecilidad = FicCDD.IdCarreraEspecilidad,
                                    IdCarrera = FicCDD.IdCarrera,
                                    IdEspecialidad= FicCDD.IdEspecialidad

                                });
                 
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }
        [HttpGet("{id}")]
        [Route("api/especialidades/especialidadescarrerasid/")]
        public async Task<ActionResult> especialidadescarrerasid(Int16 id)
        {
            try
            {
                var response = FicLoDBContext.eva_carreras_especialidades.Where(x => x.IdCarreraEspecilidad == id).ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }
    }


}