﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_API_REST.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static FicProyecto_API_REST.Models.FicCarrera;

namespace FicProyecto_API_REST.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class FicGeneralesController : ControllerBase
    {
        private readonly FicDBContext FicLoDBContext;

        public FicGeneralesController(FicDBContext FicPaDBContext)
        {

            FicLoDBContext = FicPaDBContext;

        }



        [HttpGet]
        [Route("api/generales/getgenerales/")]
        public async Task<ActionResult> GetCatGenerales()
        {
            try
            {
                var response = FicLoDBContext.cat_generales.ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }

        [HttpGet("{IdGeneral}")]
        [Route("api/generales/getcarrerasid/")]
        public async Task<ActionResult> GetGeneralID(Int16 IdGeneral)
        {
            try
            {
                var response = FicLoDBContext.cat_generales.Where(x => x.IdGeneral == IdGeneral);
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }
        [HttpPost]
        [Route("api/generales/addgeneral")]
        public ContentResult cat_GeneralesAdd([FromBody] cat_generales value)
        {
            if (value != null)
            {
                try
                {
                    try
                    {
                        var responsemax = FicLoDBContext.cat_generales.Max(x => x.IdGeneral);
                        if (responsemax != 0)
                        {
                            value.IdGeneral = (Int16)(responsemax + 1);
                        }
                        else
                        {
                            value.IdGeneral = 1;
                        }
                    }
                    catch (Exception e)
                    {
                        value.IdGeneral = 1;
                    }
                    FicLoDBContext.cat_generales.Add(value);
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }



        [HttpGet("{IdGeneral}")]
        [Route("api/generales/delete/")]
        public async Task<ActionResult> GetDeleteGeneral(Int16 IdGeneral)
        {
            try
            {
                cat_generales a = new cat_generales();
                a.IdGeneral = IdGeneral;
                FicLoDBContext.Remove(a);
                var response = FicLoDBContext.SaveChanges();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }

        [HttpPost]
        [Route("api/generales/update")]
        public ContentResult cat_generalesTipoUpdate([FromBody] cat_generales value)
        {
            if (value != null)
            {
                try
                {
                    FicLoDBContext.cat_generales.Update(value).Property(x => x.FechaReg).IsModified = false;
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }


        /*------------------------------CAT GENERALES TIPO-------------------------------------*/
        [HttpGet]
        [Route("api/generales/getgeneralestipo/")]
        public async Task<ActionResult> GetCatGeneralesTipo()
        {
            try
            {
                var response = FicLoDBContext.cat_tipos_generales.ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }

     

        [HttpPost]
        [Route("api/generales/addtipo")]
        public ContentResult cat_GeneralesTipoAdd([FromBody] cat_tipos_generales value)
        {
            if (value != null)
            {
                try
                {
                    var responsemax = FicLoDBContext.cat_tipos_generales.Max(x => x.IdTipoGeneral);

                    if (responsemax != 0)
                    {
                        value.IdTipoGeneral = (Int16)(responsemax + 1);
                    }
                    else
                    {
                        value.IdTipoGeneral = 1;
                    }
                    FicLoDBContext.cat_tipos_generales.Add(value);
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpGet("{IdTipoGeneral}")]
        [Route("api/generales/getgeneralidtipo/")]
        public async Task<ActionResult> GetGeneralTipoID(Int16 IdTipoGeneral)
        {
            try
            {
                var response = FicLoDBContext.cat_tipos_generales.Where(x => x.IdTipoGeneral == IdTipoGeneral);
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }


        [HttpPost]
        [Route("api/generales/updatetipo")]
        public ContentResult cat_generalesTipoUpdate([FromBody] cat_tipos_generales value)
        {
            if (value != null)
            {
                try
                {
                    FicLoDBContext.cat_tipos_generales.Update(value).Property(x=> x.FechaReg).IsModified=false;
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpGet("{IdTipoGeneral}")]
        [Route("api/generales/deletetipo/")]
        public async Task<ActionResult> GetDeleteID(Int16 IdTipoGeneral)
        {
            try
            {
                cat_tipos_generales a = new cat_tipos_generales();
                a.IdTipoGeneral = IdTipoGeneral;
                FicLoDBContext.Remove(a);
                var response = FicLoDBContext.SaveChanges();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }
    }
}