﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_API_REST.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static FicProyecto_API_REST.Models.FicCarrera;

namespace FicProyecto_API_REST.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class FicExportarImportarController : ControllerBase
    {
        private readonly FicDBContext FicLoDBContext;

        public FicExportarImportarController(FicDBContext FicPaDBContext)
        {

            FicLoDBContext = FicPaDBContext;

        }

        [Route("api/importar/getall")]
        public async Task<ActionResult> getall()
        {
              
            try
            {
                cat_exportar_importar temp = new cat_exportar_importar();
                temp.eva_cat_carreras = FicLoDBContext.eva_cat_carreras.ToList();
                temp.cat_generales = FicLoDBContext.cat_generales.ToList();
                temp.cat_tipos_generales = FicLoDBContext.cat_tipos_generales.ToList();
                temp.eva_cat_especialidades = FicLoDBContext.eva_cat_especialidades.ToList();
                temp.eva_carreras_especialidades = FicLoDBContext.eva_carreras_especialidades.ToList();
                temp.eva_cat_reticulas = FicLoDBContext.eva_cat_reticulas.ToList();
                temp.eva_carreras_reticulas = FicLoDBContext.eva_carreras_reticulas.ToList();
                 
                return Ok(temp);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }

        [HttpPost]
        [Route("api/exportar/setall")]
        //publico retorna un resultado su nombre es PostEdi y se obtiene un body
        public async Task<ActionResult> setall([FromBody] cat_exportar_importar value)
        {
            if (value != null)
            {
                try
                {
                    foreach (cat_tipos_generales item in (value.cat_tipos_generales))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.cat_tipos_generales
                                         where FicCED.IdTipoGeneral == item.IdTipoGeneral
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.cat_tipos_generales.Update(item);
                           
                        }
                        else
                        {
                            FicLoDBContext.cat_tipos_generales.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<cat_tipos_generales>(item).State = EntityState.Detached;
                    }
                    foreach (cat_generales item in (value.cat_generales))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.cat_generales
                                         where FicCED.IdGeneral == item.IdGeneral
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.cat_generales.Update(item);
                        }
                        else
                        {
                            FicLoDBContext.cat_generales.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<cat_generales>(item).State = EntityState.Detached;
                    }
                    foreach (eva_cat_carreras item in (value.eva_cat_carreras))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.eva_cat_carreras
                                         where FicCED.IdCarrera == item.IdCarrera
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.eva_cat_carreras.Update(item);
                        }
                        else
                        { 
                            FicLoDBContext.eva_cat_carreras.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<eva_cat_carreras>(item).State = EntityState.Detached;
                    }
                    foreach (eva_cat_especialidades item in (value.eva_cat_especialidades))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.eva_cat_especialidades
                                         where FicCED.IdEspecialidad == item.IdEspecialidad
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.eva_cat_especialidades.Update(item);
                        }
                        else
                        {
                            FicLoDBContext.eva_cat_especialidades.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<eva_cat_especialidades>(item).State = EntityState.Detached;
                    }
                    foreach (eva_carreras_especialidades item in (value.eva_carreras_especialidades))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.eva_carreras_especialidades
                                         where FicCED.IdCarreraEspecilidad == item.IdCarreraEspecilidad
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.eva_carreras_especialidades.Update(item);
                        }
                        else
                        {
                            FicLoDBContext.eva_carreras_especialidades.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<eva_carreras_especialidades>(item).State = EntityState.Detached;
                    }
                    foreach (eva_cat_reticulas item in (value.eva_cat_reticulas))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.eva_cat_reticulas
                                         where FicCED.IdReticula == item.IdReticula
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.eva_cat_reticulas.Update(item);
                        }
                        else
                        {
                            FicLoDBContext.eva_cat_reticulas.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<eva_cat_reticulas>(item).State = EntityState.Detached;

                    }
                    foreach (eva_carreras_reticulas item in (value.eva_carreras_reticulas))
                    {
                        var FicResult = (from FicCED in FicLoDBContext.eva_carreras_reticulas
                                         where FicCED.IdCarreraRiticula == item.IdCarreraRiticula
                                         select new
                                         {
                                             FicCED
                                         }).Count() > 0;

                        if (FicResult)
                        {
                            FicLoDBContext.eva_carreras_reticulas.Update(item);
                        }
                        else
                        {
                            FicLoDBContext.eva_carreras_reticulas.Add(item);
                        }
                        FicLoDBContext.SaveChanges();
                        FicLoDBContext.Entry<eva_carreras_reticulas>(item).State = EntityState.Detached;

                    }
                    return Ok();
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return NotFound("Error");
                }
            }
            else
            {
                return NotFound("Error");
            }
        }


    }
}