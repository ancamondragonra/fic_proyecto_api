﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_API_REST.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static FicProyecto_API_REST.Models.FicCarrera;

namespace FicProyecto_API_REST.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class FicCarrerasController : ControllerBase
    {
        private readonly FicDBContext FicLoDBContext;

        public FicCarrerasController(FicDBContext FicPaDBContext)
        {

            FicLoDBContext = FicPaDBContext;

        }


        [HttpGet]
        [Route("api/carreras/getcarreras/")]
        public async  Task<ActionResult> GetCarreras()
        {
            try
            {
                var response = FicLoDBContext.eva_cat_carreras.ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }

        [HttpGet("{IdCarrera}")]
        [Route("api/carreras/getcarrerasid/")]
        public async Task<ActionResult> GetCarrerasID(Int16 IdCarrera)
        {
            try
            {
                var response = FicLoDBContext.eva_cat_carreras.Where(x => x.IdCarrera == IdCarrera);
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }

        [HttpGet]
        [Route("api/carreras/getgenerales/")]
        public async Task<ActionResult> GetCatGenerales()
        {
            try
            {
                var response = FicLoDBContext.cat_generales.ToList();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }
        }

    

        [HttpPost]
        [Route("api/carreras/add")]
        public ContentResult cat_carrerasAdd([FromBody] eva_cat_carreras value)
        {
            if (value != null)
            {
                try
                {
                    try
                    {
                        var responsemax = FicLoDBContext.eva_cat_carreras.Max(x => x.IdCarrera);
                        if (responsemax != 0)
                        {
                            value.IdCarrera = (Int16)(responsemax + 1);
                        }
                        else
                        {
                            value.IdCarrera = 1;
                        }
                    }
                    catch (Exception e)
                    {
                        value.IdCarrera = 1;
                    }


                    var GenGrado = FicLoDBContext.cat_generales.Where(x => x.IdGeneral == value.IdGenGradoEscolar).First();
                    var GenModalidad = FicLoDBContext.cat_generales.Where(x => x.IdGeneral == value.IdGenModalidad).First();

                    value.UsuarioReg = "FicIbarra";
                    value.UsuarioMod = "FicIbarra";
                    value.IdTipoGenGradoEscolar = GenGrado.IdTipoGeneral;
                    value.IdTipoGenModalidad = GenModalidad.IdTipoGeneral;

                    FicLoDBContext.eva_cat_carreras.Add(value);
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpPost]
        [Route("api/carreras/update")]
        public ContentResult cat_carrerasUpdate([FromBody] eva_cat_carreras value)
        {
            if (value != null)
            {
                try
                {
                    value.UsuarioReg = "FicIbarra";
                    value.UsuarioMod = "FicIbarra";
                    FicLoDBContext.eva_cat_carreras.Update(value).Property(x=> x.FechaReg).IsModified=false;
                    var response = FicLoDBContext.SaveChanges();
                    return Content("{'response':'Successful','row_insert':'" + response + "'}", "application/json");
                }
                catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    return Content("{'response':'error','Exception':'" + e.Message + "'}", "application/json");
                }
            }
            else
            {
                return Content("{'response':'error'}", "application/json");
            }
        }

        [HttpGet("{IdCarrera}")]
        [Route("api/carreras/delete/")]
        public async Task<ActionResult> GetDeleteID(Int16 IdCarrera)
        {
            try
            {
                eva_cat_carreras a = new eva_cat_carreras();
                a.IdCarrera = IdCarrera;
                FicLoDBContext.Remove(a);
                var response = FicLoDBContext.SaveChanges();
                return Ok(response);
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException e)
            {
                return NotFound("Sin Edificios");
            }

        }
    }
}