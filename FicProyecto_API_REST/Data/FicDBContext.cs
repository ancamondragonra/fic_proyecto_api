﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static FicProyecto_API_REST.Models.FicCarrera;

namespace FicProyecto_API_REST.Data
{
    public class FicDBContext : DbContext
    {

        public FicDBContext(DbContextOptions<FicDBContext> options): base(options)
        {
            //instancia con los parametros que recibe
        }//constructor
        public DbSet<eva_cat_carreras> eva_cat_carreras { get; set; }
        public DbSet<cat_generales> cat_generales { get; set; }
        public DbSet<cat_tipos_generales> cat_tipos_generales { get; set; }
        public DbSet<eva_cat_especialidades> eva_cat_especialidades { get; set; }

        public DbSet<eva_carreras_especialidades> eva_carreras_especialidades { get; set; }
        public DbSet<eva_carreras_reticulas> eva_carreras_reticulas { get; set; }
        public DbSet<eva_cat_reticulas> eva_cat_reticulas { get; set; }

        protected async override void OnModelCreating(ModelBuilder modelBuilder)
        {
            try
            {


                #region eva_cat_carreras
                modelBuilder.Entity<eva_cat_carreras>()
                .HasKey(c => new { c.IdCarrera });

                /* modelBuilder.Entity<eva_cat_carreras>()
                  .HasOne(s => s.rh_cat_areas_deptos).
                  WithMany().HasForeignKey(s => new { s.IdAreaDepto });*/

                modelBuilder.Entity<eva_cat_carreras>()
               .HasOne(s => s.cat_tipos_generales).
              WithMany().HasForeignKey(s => new { s.IdTipoGenGradoEscolar }).OnDelete(DeleteBehavior.Restrict);
                modelBuilder.Entity<eva_cat_carreras>()
               .HasOne(s => s.cat_generales).
               WithMany().HasForeignKey(s => new { s.IdGenGradoEscolar }).OnDelete(DeleteBehavior.Restrict);
                modelBuilder.Entity<eva_cat_carreras>()
               .HasOne(s => s.cat_tipos_generales_modalidad).
               WithMany().HasForeignKey(s => new { s.IdTipoGenModalidad }).OnDelete(DeleteBehavior.Restrict);
                modelBuilder.Entity<eva_cat_carreras>()
               .HasOne(s => s.cat_generales_modalidad).
               WithMany().HasForeignKey(s => new { s.IdGenModalidad }).OnDelete(DeleteBehavior.Restrict);


                #endregion

                #region eva_cat_especialidades
                modelBuilder.Entity<eva_cat_especialidades>()
                  .HasKey(c => new { c.IdEspecialidad });
                #endregion

                #region eva_carreras_especialidades

                modelBuilder.Entity<eva_carreras_especialidades>()
                 .HasKey(c => new { c.IdCarreraEspecilidad });
                modelBuilder.Entity<eva_carreras_especialidades>()
                .HasOne(s => s.eva_cat_carreras).
                WithMany().HasForeignKey(s => new { s.IdCarrera });
                modelBuilder.Entity<eva_carreras_especialidades>()
                .HasOne(s => s.eva_cat_especialidades).
                WithMany().HasForeignKey(s => new { s.IdEspecialidad }).OnDelete(DeleteBehavior.Restrict);

                #endregion


                #region cat_generales
                modelBuilder.Entity<cat_generales>()
                .HasKey(c => new { c.IdGeneral });
                #endregion


                #region eva_cat_reticulas
                modelBuilder.Entity<eva_cat_reticulas>()
                .HasKey(c => new { c.IdReticula });

                #endregion

                #region eva_carreras_reticulas
                modelBuilder.Entity<eva_carreras_reticulas>()
                .HasKey(c => new { c.IdCarreraRiticula });

                #endregion
            }
            catch (Exception e)
            {
               
            }
        }//al crear el modelo
    }
}
