USE FIC_PROYECTO;

CREATE TABLE cat_tipo_generales
( 
	IdTipoGeneral        smallint  NOT NULL primary key,
	DesTipo              varchar(100) NULL ,
	Activo               char(1)  NULL ,
	FechaReg             datetime  NULL ,
	UsuarioReg           varchar(20)  NULL ,
	FechaUltMod          datetime  NULL ,
	UsuarioMod           varchar(20)  NULL ,
	Borrado              char(1)  NULL 
)
	
go

CREATE TABLE cat_generales
( 
	IdGeneral            smallint  NOT NULL primary key,
	IdTipoGeneral        smallint  NOT NULL ,
	Clave				 varchar (20) NULL,
	DesGeneral			 varchar (100) NULL,
	IdLlaveClasifica     varchar (50) NULL,
	Referencia			 varchar (50) NULL,
	Activo               char(1)  NULL ,
	FechaReg             datetime  NULL ,
	UsuarioReg           varchar(20)  NULL ,
	FechaUltMod          datetime  NULL ,
	UsuarioMod           varchar(20)  NULL ,
	Borrado              char(1)  NULL ,
  /*  constraint fk_IdTipoGeneral_g foreign key (IdTipoGeneral) references cat_tipo_generales (IdTipoGeneral)*/
)
go

ALTER TABLE cat_generales
ADD CONSTRAINT fk_IdTipoGeneral_g FOREIGN KEY (IdTipoGeneral)
    REFERENCES cat_tipo_generales(IdTipoGeneral)
go



CREATE TABLE eva_cat_carreras
( 
	IdCarrera            smallint  NOT NULL primary key,
	ClaveCarrera         varchar(20)  NULL ,
	ClaveOficial         varchar(20)  NULL ,
	DesCarrera           varchar(100)  NULL ,
	Alias                varchar (10)  NULL ,
	IdTipoGenGradoEscolar smallint NOT NULL,
	IdGenGradoEscolar    smallint NOT NULL,
	FechaReg             datetime  NULL ,
	FechaUltMod          datetime  NULL ,
	UsuarioReg           varchar(20)  NULL ,
	UsuarioMod           varchar(20)  NULL ,
	Activo               char(1)  NULL ,
	Borrado              char(1)  NULL ,
	NombreCorto          varchar(20) NULL,
	Creditos             integer NULL,
	IdTipoGenModalidad   smallint NOT NULL,
	IdGenModalidad       smallint NOT NULL,
	FechaIni             datetime  NULL ,
	FechaFin             datetime  NULL,
	constraint fk_IdTipoGenGradoEscolar_c foreign key (IdTipoGenGradoEscolar) references cat_tipo_generales (IdTipoGeneral),
    constraint fk_IdGeneral_c foreign key (IdGenGradoEscolar) references cat_generales (IdGeneral),
	constraint fk_IdTipoGenModalidad_c foreign key (IdTipoGenModalidad) references cat_tipo_generales (IdTipoGeneral),
    constraint fk_IdGenModalidad_c foreign key (IdGenModalidad) references cat_generales (IdGeneral)
)
go


	
CREATE TABLE eva_cat_especialidades
( 
	IdEspecialidad       smallint  NOT NULL primary key,
	DesEspecialidad      varchar(20)  NULL ,
	Activo               char(1)  NULL ,
	Borrado              char(1)  NULL ,
	FechaReg             datetime  NULL ,
	UsuarioReg           varchar(20)  NULL ,
	FechaUltMod          datetime  NULL ,
	UsuarioMod           varchar(20)  NULL 
	
)
go

CREATE TABLE eva_carreras_especialidades
( 
	IdEspecialidad       smallint  NOT NULL ,
	IdCarrera            smallint  NOT NULL ,
	Activo               char(1)  NULL ,
	Borrado              char(1)  NULL ,
	FechaIni             datetime  NULL ,
	FechaFin             datetime  NULL ,
	FechaReg             datetime  NULL ,
	UsuarioReg			 varchar (20) NULL ,
	FechaUltMod			 datetime NULL ,
	UsuarioMod			 varchar (20) NULL, 
	constraint fk_IdEspecialidad_e  foreign key (IdEspecialidad ) references eva_cat_especialidades (IdEspecialidad ),
	constraint fk_IdCarrera_e  foreign key (IdCarrera ) references eva_cat_carreras (IdCarrera )
)
go


CREATE TABLE eva_cat_reticulas
( 
	IdReticula       smallint NOT NULL primary key,
	DesArea           varchar(255) NULL ,
	NoArea			  varchar (10) NULL,
	IdAreaDeptoPadre    smallint NOT NULL,
	IdTipoGenArea     smallint NOT NULL,
	IdGenArea		 smallint NOT NULL,
	Nivel			 smallint NOT NULL,
	FechaReg             datetime  NULL ,
	UsuarioReg           varchar(20)  NULL ,
	FechaUltMod          datetime  NULL ,
	UsuarioMod           varchar(20)  NULL ,
	Activo               char(1)  NULL ,
	Borrado              char(1)  NULL 
)
	
go

CREATE TABLE eva_carreras_reticulas
( 
	IdCarrea             smallint  NOT NULL ,
	IdReticula           smallint  NOT NULL ,
	Activo               char(1)  NULL ,
	Borrado              char(1)  NULL ,
	FechaReg             datetime  NULL ,
	UsuarioReg           varchar(20)  NULL ,
	FechaUltMod          datetime  NULL ,
	UsuarioMod           varchar(20)  NULL,
	constraint fk_IdCarrera_r  foreign key (IdCarrea ) references eva_cat_carreras (IdCarrera ),
	constraint fk_IdReticula_r foreign key (IdReticula ) references eva_cat_reticulas (IdReticula )
)
go


/*
ALTER TABLE eva_cat_carreras
	ADD CONSTRAINT XPKeva_cat_carreras PRIMARY KEY (IdCarrera ASC)
go



CREATE INDEX XIF1eva_cat_carreras ON eva_cat_carreras
( 
	IdCarrera         ASC
)
go

ALTER TABLE eva_cat_especialidades
	ADD CONSTRAINT XPKcat_tipos_estatus PRIMARY KEY (IdEspecialidad  ASC)
go




ALTER TABLE cat_tipo_generales
	ADD CONSTRAINT XPKcat_tipo_generales PRIMARY KEY (IdTipoGeneral   ASC)
go



ALTER TABLE cat_generales
	ADD CONSTRAINT XPKcat_generales PRIMARY KEY (IdGeneral    ASC)
go



ALTER TABLE eva_cat_carreras
	ADD CONSTRAINT R_8 FOREIGN KEY (IdGenModalidad,IdGenGradoEscolar) REFERENCES cat_generales(IdTipoGenGradoEscolar,IdTipoGenGradoEscolar)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE eva_cat_carreras
	ADD CONSTRAINT R_164 FOREIGN KEY (IdTipoGenModalidad,IdTipoGenGradoEscolar) REFERENCES cat_tipo_generales(IdTipoGenModalidad,IdTipoGenGradoEscolar)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE eva_carreras_especialidades
	ADD CONSTRAINT R_811 FOREIGN KEY (IdCarrera) REFERENCES eva_cat_carreras(IdCarrera)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE eva_carreras_reticulas
	ADD CONSTRAINT R_811 FOREIGN KEY (IdCarrera) REFERENCES eva_cat_carreras(IdCarrera)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE eva_carreras_reticulas
	ADD CONSTRAINT R_811 FOREIGN KEY (IdReticula) REFERENCES eva_cat_reticulas(IdReticula)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE cat_generales
	ADD CONSTRAINT R_811 FOREIGN KEY (IdTipoGeneral) REFERENCES cat_tipos_generales(IdTipoGeneral)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go
*/